### **Brainburn programming language**

## Backend C variables:
| Type  | Name |  Descritpion |
|--|--|--|
| FILE | fout | standart output file (default stdout)|
| FILE | ferr | standart error output file (default stderr) |
| FILE | fin |	standart input file (default stdin) |
| char | *ptr | standart pointer |
| char | str | standart temp value |
| char | *argv[] | argument array |
| int | argc | total argument length |
|int |memsize|calloc memory size **(static)**|

## Language overview:
Brainburn based on brainfuck & brainlove but brainburn has extra commands.

|Initial C code|
|--|
|FILE* **fout**;|
|FILE* **ferr**;|
|FILE* **fin**;|
|int **main**(int argc,char *argv[]){|
|unsigned char ***array**=calloc(memsize, 1);|
|unsigned char ***ptr**=**array**;|
|unsigned char **str**=0;|
|**fout**=**stdout**;|
|**ferr**=**stderr**;|
|**fin**=**stdin**;|

|End of C code|
|--|
|return str;|
|}|

|Brainburn | C | Description| Is extra|
|--|--|--|--|
|<|`ptr=ptr+1;`|Move the pointer to the right |No|
|<|`ptr=ptr-1;`|Move the pointer to the left |No|
|+|`*ptr=*ptr+1;`|Increment the memory cell under the pointer|No|
|-|`*ptr=*ptr-1;`|Decrement the memory cell under the pointer|No|
|.|`fprintf(fout,"%c",*ptr);`|Output the character signified by the cell at the pointer|No|
|?|`fprintf(ferr,"%c",*ptr);`|Output error the character signified by the cell at the pointer|**Yes**|
|,|`fscanf(fin,"%c",ptr);`|Input a character and store it in the cell at the pointer|No|
|"|`fprintf(fout,"%d",(int)*ptr);`|Output the integer signified by the cell at the pointer|**Yes**|
|`|`fprintf(ferr,"%d",(int)*ptr);`|Output error the integer signified by the cell at the pointer|**Yes**|
|'|`fscanf(fin,"%d",ptr);`|Input a character and store it in the cell at the pointer|**Yes**|
|[|`while (*ptr != str) {`|Jump past the matching ] if the cell under the pointer is not str|No|
|(|`while (*ptr == str) {`|Jump past the matching ) if the cell under the pointer is str|**Yes**|
|) } ]|`}`|Complete if or while segment|**Yes** (] is not extra character)|
|~|`break;`|Break current while or if segment| **Yes**|
|$|`str=*ptr;`|Store *ptr in storage|**Yes**|
|!|`*ptr=str;`|return from storage|**Yes**|
|{|`if (*ptr != str) {`|Past if the cell under the pointer is not str |**Yes**|
|*|`*ptr=*ptr*2;`|Multiplies *ptr and 2|**Yes**|
|^|`*ptr=*ptr*str;`|Multiplies *ptr and str|**Yes**|
|/|`*ptr=*ptr/2;`|Divides *ptr tr 2|**Yes**|
|\||`}else{`|Past if the cell under the pointer is 0|**Yes**|
|#|``|raw C source code line|**Yes**|
|0|`*ptr=0;`|*ptr equal to 0|**Yes**|
|O|`str=0;`|str equal to 0|**Yes**|
|N|`fprintf(ferr,"%c","\n");`|Write new line to output error|**Yes**|
|n|`fprintf(fout,"%c","\n");`|Write new line to output|**Yes**|

#  Hello World in Brainburn

`#// H=72= (2*2*2+1)*(2*2*2)`

`+***+***.0`

`#// e=101= ((((2+1)*(2*2*2))+1)*(2*2))+1`

`+*+***+**+.`

`#// l=108 = e+7`

`+++++++..`

`#// o=111 = l+3 store to memory`

`+++.$0`

`#// backspace=32 =(2*2*2*2*2)`

`+*****.0`

`#// W=87=(((((((2*2)+1)*2*2)+1)*2)+1)*2)+1`

`+**+**+*+*+.`

`#// o=111 restore from memory`

`!.`

`#// r=114=o+3`

`+++.`

`#// l=108=r-6`

`------.*`

`#//d=100=(((2+1)*(2*2*2))+1)*(2*2)`

`+*+***+**.`

`#//new line`

`n`


# Cat in Brainburn

`+[,.]`

# Compiling:
make BF=bf_src/helloworld.bf

