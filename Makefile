CC=gcc
CFLAG=-Isrc -O3 -s -o
OUT=build
NAME="bf"
BF=bf_src/helloworld.bf
all: clean build
build:
	mkdir -p $(OUT)
	$(CC) $(CFLAG) $(OUT)/bf src/bf.c
	cat $(BF) | $(OUT)/bf > $(OUT)/$(NAME).c
	$(CC) $(CFLAG) $(OUT)/$(NAME) $(OUT)/$(NAME).c
clean:
	rm -rf build/
install:
	install $(OUT)/$(NAME) $(TARGET)/usr/bin/$(NAME) 
