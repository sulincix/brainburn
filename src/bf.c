#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int memsize=1024000;
int comment=0;
int main(int argc,char *argv[]){
  char bf[1];
  fprintf(stdout,"#include <stdio.h>\n");
  fprintf(stdout,"#include <stdlib.h>\n");
  for(int i=1;i!=argc;i++){
    fprintf(stdout,"#include <%s>\n",argv[i]);
  }

  fprintf(stdout,"\tFILE* fout;\n");
  fprintf(stdout,"\tFILE* ferr;\n");
  fprintf(stdout,"\tFILE* fin;\n");
  fprintf(stdout,"\nint main(int argc,char *argv[]){\n");
  fprintf(stdout,"\tunsigned char *array=calloc(%d, 1);\n",memsize);
  fprintf(stdout,"\tunsigned char *ptr=array;\n");
  fprintf(stdout,"\tunsigned char str=0;\n");
  fprintf(stdout,"\tfout=stdout;\n");
  fprintf(stdout,"\tferr=stderr;\n");
  fprintf(stdout,"\tfin=stdin;\n");
  while(!feof(stdin)){
    bf[0]=fgetc(stdin);
    if(comment==0){
      if(bf[0]==(int)'<'){
        fprintf(stdout,"\t++ptr;\n");
      }
      if(bf[0]==(int)'>'){
        fprintf(stdout,"\t--ptr;\n");
      }
      if(bf[0]==(int)'+'){
        fprintf(stdout,"\t++*ptr;\n");
      }
      if(bf[0]==(int)'-'){
        fprintf(stdout,"\t--*ptr;\n");
      }
      if(bf[0]==(int)'*'){
        fprintf(stdout,"\t*ptr=*ptr*2;\n");
      }
      if(bf[0]==(int)'^'){
        fprintf(stdout,"\t*ptr=(*ptr)*str;\n");
      }
      if(bf[0]==(int)'/'){
        fprintf(stdout,"\t*ptr=*ptr/2;\n");
      }
      if(bf[0]==(int)'|'){
        fprintf(stdout,"\t}else{;\n");
      }
      if(bf[0]==(int)'.'){
        fprintf(stdout,"\tfprintf(fout,\"%sc\",*ptr);\n","%");
      }
      if(bf[0]==(int)'"'){
        fprintf(stdout,"\tfprintf(fout,\"%sd\",(int)*ptr);\n","%");
      }
      if(bf[0]==(int)'`'){
        fprintf(stdout,"\tfprintf(ferr,\"%sd\",(int)*ptr);\n","%");
      }
      if(bf[0]==(int)'\''){
        fprintf(stdout,"\tfscanf(fin,\"%sd\",ptr);\n","%");
      }
      if(bf[0]==(int)'?'){
        fprintf(stdout,"\tfprintf(ferr,\"%sc\",*ptr);\n","%");
      }
      if(bf[0]==(int)'N'){
        fprintf(stdout,"\tfprintf(ferr,\"\\n\");\n");
      }
      if(bf[0]==(int)'n'){
        fprintf(stdout,"\tfprintf(fout,\"\\n\");\n");
      }
     if(bf[0]==(int)','){
        fprintf(stdout,"\tfscanf(fin,\"%sc\",ptr);\n","%");
      }
      if(bf[0]==(int)'['){
        fprintf(stdout,"\twhile (*ptr != str) {\n");
      }
      if(bf[0]==(int)'{'){
        fprintf(stdout,"\tif (*ptr != str) {\n");
      }
      if(bf[0]==(int)'('){
        fprintf(stdout,"\twhile (*ptr == str) {\n");
      }
      if(bf[0]==(int)'~'){
        fprintf(stdout,"\tbreak;\n");
      }
      if(bf[0]==(int)'0'){
        fprintf(stdout,"\t*ptr=0;\n");
      }
      if(bf[0]==(int)'O'){
        fprintf(stdout,"\tstr=0;\n");
      }
      if(bf[0]==(int)'$'){
        fprintf(stdout,"\tstr=*ptr;\n");
      }
      if(bf[0]==(int)'&'){
        fprintf(stdout,"\tbreak;\n");
      }
      if(bf[0]==(int)'!'){
        fprintf(stdout,"\t*ptr=str;\n");
      }
      if(bf[0]==(int)']' || bf[0]==(int)')'|| bf[0]==(int)'}'){
        fprintf(stdout,"\t}\n");
      }
      if(bf[0]==(int)'#'){
        comment=1;
      }
    }else{
      if(bf[0]==(int)'\n'){
        comment=0;
      }
      fprintf(stdout,"%c",bf[0]);
    }
    strcpy(bf,"");
  }
  fprintf(stdout,"return str;\n");
  fprintf(stdout,"}\n");
  return 0;
}
